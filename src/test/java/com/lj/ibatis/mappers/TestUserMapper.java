package com.lj.ibatis.mappers;

import com.lj.ibatis.entity.TestUser;

import java.io.Serializable;

/**
 * @author luojing
 * @date 2023/12/7
 */
public interface TestUserMapper {

  TestUser selectById(Serializable id);
}
