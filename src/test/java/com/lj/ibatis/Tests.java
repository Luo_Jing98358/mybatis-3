package com.lj.ibatis;

import com.lj.ibatis.entity.TestUser;
import com.lj.ibatis.mappers.TestUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author luojing
 * @date 2023/12/7
 */
@Slf4j
public class Tests {

  @Test
  public void test() throws IOException {
    String resource = "mybatis-config.xml";
    InputStream inputStream = Resources.getResourceAsStream(resource);
    SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
    SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);
    try (SqlSession session = sqlSessionFactory.openSession()) {
      // 这里实际是拿到的TestUserMapper的代理对象MapperProxy
      TestUserMapper testUserMapper = session.getMapper(TestUserMapper.class);
      // 看看怎么执行的呢
      TestUser testUser = testUserMapper.selectById(1);
      System.out.println(testUser);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
