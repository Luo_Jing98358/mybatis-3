package com.lj.ibatis.entity;

import lombok.Data;

/**
 * @author luojing
 * @date 2023/12/7
 */
@Data
public class TestUser {
  private Integer id;
  private String userName;
  private Integer age;
}
